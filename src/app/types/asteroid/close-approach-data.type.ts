import { MissDistance, RelativeVelocity } from './';

export interface CloseApproachData {
  close_approach_date: string;
  epoch_date_close_approach: number;
  miss_distance: MissDistance;
  orbiting_body: string;
  relative_velocity: RelativeVelocity;
}
