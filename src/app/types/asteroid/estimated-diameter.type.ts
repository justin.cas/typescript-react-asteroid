import { EstimatedDiameterValues } from './';

export interface EstimatedDiameter {
  kilometers: EstimatedDiameterValues;
  meters: EstimatedDiameterValues;
  miles: EstimatedDiameterValues;
  feet: EstimatedDiameterValues;
}
