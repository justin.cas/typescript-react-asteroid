import { Links, EstimatedDiameter, CloseApproachData } from './';

export interface Asteroid {
  absolute_magnitude_h: number;
  close_approach_data: CloseApproachData[];
  estimated_diameter: EstimatedDiameter;
  is_potentially_hazardous_asteroid: boolean;
  links: Links;
  name: string;
  nasa_jpl_url: string;
  neo_reference_id: string;
}
