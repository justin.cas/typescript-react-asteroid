import { Middleware } from 'redux';
import { SagaMiddleware } from 'redux-saga';
import createSagaMiddleware from 'redux-saga';

export const saga: SagaMiddleware<Middleware> = createSagaMiddleware();
