import 'isomorphic-fetch';

import { take, put, call, apply } from 'redux-saga/effects';
import { receiveAsteroids, AsteroidActions } from 'app/actions';

const apiKey = 'fsuLz58GK5TCb1gaOK4hOO5RQGPdRH4ZlVIXIOZb';

export function* fetchAsteroids() {
  yield take(AsteroidActions.Type.REQUEST_ASTEROIDS);
  const response = yield call(
    <any>fetch,
    `https://api.nasa.gov/neo/rest/v1/feed?start_date=2015-09-07&end_date=2015-09-08&api_key=${apiKey}`,
    {
      mode: 'cors'
    }
  );
  const data = yield apply(response, response.json);
  yield put(receiveAsteroids(data));
}
