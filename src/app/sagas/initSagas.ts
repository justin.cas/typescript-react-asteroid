import * as sagas from 'app/sagas';
import { Middleware } from 'redux';
import { SagaMiddleware } from 'redux-saga';

export const initSagas = (sagaMiddleware: SagaMiddleware<Middleware>): void => {
  console.log('init sagas');
  (<any>Object).values(sagas).forEach(sagaMiddleware.run.bind(sagaMiddleware));
};
