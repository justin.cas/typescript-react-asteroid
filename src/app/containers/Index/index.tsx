import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { RootState } from 'app/reducers';
import { Header } from 'app/components';
import { Asteroids } from 'app/components/asteroids';
import { Store } from 'redux';
import { requestAsteroids } from 'app/actions';
import { Asteroid as AsteroidModel } from 'app/types/asteroid';

export namespace Index {
  export interface Props extends RouteComponentProps<void> {
    asteroids: AsteroidModel[];
    dispatch: Dispatch<Store<any>>;
  }
}

@connect(
  (state: RootState): Pick<Index.Props, 'asteroids'> => {
    return { asteroids: state.asteroids };
  }
)
export class Index extends React.Component<Index.Props> {
  constructor(props: Index.Props, context?: any) {
    super(props, context);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(requestAsteroids());
  }

  render() {
    const { asteroids } = this.props;
    return (
      <div>
        <Header />
        <Asteroids asteroids={asteroids} />
      </div>
    );
  }
}
