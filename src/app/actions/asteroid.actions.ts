import { Asteroid } from 'app/types/asteroid';

export namespace AsteroidActions {
  export enum Type {
    REQUEST_ASTEROIDS = 'REQUEST_ASTEROIDS',
    RECEIVE_ASTEROIDS = 'RECEIVE_ASTEROIDS',
    REQUEST_ASTEROID = 'REQUEST_ASTEROID',
    RECEIVE_ASTEROID = 'RECEIVE_ASTEROID'
  }
}

export const requestAsteroids = () => {
  return {
    type: AsteroidActions.Type.REQUEST_ASTEROIDS
  };
};

export const receiveAsteroids = (asteroids: Asteroid[]) => {
  return {
    type: AsteroidActions.Type.RECEIVE_ASTEROIDS,
    asteroids
  };
};

export const requestAsteroid = (id: string, apiKey: 'string') => {
  return {
    type: AsteroidActions.Type.REQUEST_ASTEROID,
    id
  };
};

export const receiveAsteroid = (asteroids: Asteroid[]) => {
  return {
    type: AsteroidActions.Type.RECEIVE_ASTEROID,
    asteroids
  };
};

export type AsteroidActions = Omit<typeof AsteroidActions, 'Type'>;
