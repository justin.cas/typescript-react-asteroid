import { combineReducers, Reducer } from 'redux';
import { RootState } from 'app/reducers/state';
import { routerReducer, RouterState } from 'react-router-redux';
import { asteroidsReducer } from 'app/reducers/asteroids.reducer';

export { RootState, RouterState };

export const rootReducer: Reducer<RootState> = combineReducers<RootState>({
  router: routerReducer as Reducer<RouterState>,
  asteroids: asteroidsReducer as Reducer<any>
});
