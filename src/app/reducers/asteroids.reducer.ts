import { RootState } from 'app/reducers/state';
import { handleActions } from 'redux-actions';
import { AsteroidActions } from 'app/actions/asteroid.actions';

const initialState: RootState.AsteroidState = [];

export const asteroidsReducer = handleActions<RootState.AsteroidState>(
  {
    [AsteroidActions.Type.RECEIVE_ASTEROIDS]: (state, action: any) => {
      return action.asteroids.near_earth_objects['2015-09-07'];
    },
    [AsteroidActions.Type.RECEIVE_ASTEROID]: (state, action: any) => {
      return action.asteroid;
    }
  },
  initialState
);
