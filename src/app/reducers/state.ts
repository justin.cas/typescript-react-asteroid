import { RouterState } from 'react-router-redux';
import { Asteroid as AsteroidModel } from 'app/types/asteroid';

export interface RootState {
  asteroids: RootState.AsteroidState;
  router: RouterState;
}

export namespace RootState {
  export type AsteroidState = AsteroidModel[];
}
