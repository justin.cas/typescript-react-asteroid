import * as React from 'react';
import { Asteroid as AsteroidModel } from 'app/types/asteroid';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export namespace Asteroid {
  export interface Props {
    asteroid: AsteroidModel;
  }
}

export class Asteroid extends React.Component<Asteroid.Props> {
  protected readonly unit = 'mph';

  constructor(props: Asteroid.Props, context?: any) {
    super(props, context);
  }

  formatSpeed(velocity: string, unit?: string): string {
    return (Math.round(Number(velocity) * 100) / 100).toFixed(2);
  }

  render(): JSX.Element {
    const { asteroid } = this.props;
    const { orbiting_body, relative_velocity }: any = asteroid.close_approach_data.map(
      (item) => item
    )[0];

    return (
      <div className="asteroid">
        <Card className={''}>
          <CardMedia
            className={''}
            image="http://via.placeholder.com/350x150"
            title="Placeholder"
          />
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              {asteroid.name}
            </Typography>
            <Typography component="p">
              Orbiting: {orbiting_body} at {this.formatSpeed(relative_velocity.miles_per_hour)}{' '}
              miles per hour
            </Typography>
            <Typography component="p">
              Hazardous: {asteroid.is_potentially_hazardous_asteroid.toString()}
            </Typography>
          </CardContent>
          <CardActions>
            <Button size="small" color="primary">
              <a href={asteroid.nasa_jpl_url}>More Info</a>
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}
