import * as React from 'react';
import { Asteroid as AsteroidModel } from 'app/types/asteroid';
import { Asteroid } from 'app/components/asteroid';

export namespace Asteroids {
  export interface Props {
    asteroids: AsteroidModel[];
  }
}

export class Asteroids extends React.Component<Asteroids.Props> {
  constructor(props: Asteroids.Props, context?: any) {
    super(props, context);
  }

  render(): JSX.Element | null {
    const { asteroids } = this.props;

    if (!asteroids) {
      return null;
    }

    return (
      <div>
        <ul>
          {asteroids.map((asteroid) => (
            <Asteroid key={asteroid.neo_reference_id} asteroid={asteroid} />
          ))}
        </ul>
      </div>
    );
  }
}
