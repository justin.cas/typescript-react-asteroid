import * as React from 'react';

export const Header = () => (
  <header>
    <h1>Near Earth Objects</h1>
  </header>
);
