import * as React from 'react';
import { Route, Switch } from 'react-router';
import { Index as _Index } from 'app/containers/Index';
import { hot } from 'react-hot-loader';

export const App = hot(module)(() => (
  <Switch>
    <Route path="/" component={_Index} />
  </Switch>
));
